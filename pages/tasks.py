from celery.task import periodic_task
from datetime import timedelta
from pages.controllers import VkApi
from models import Message
import requests

api_vk = VkApi()
new_count = Message(id=1, msg_count=api_vk.get_msg_count())
new_count.save()

@periodic_task(run_every = timedelta(seconds = 20))
def messages_reload():

    get_message_api = api_vk.get_msg_count()
    get_cur_msg = Message.objects.get(id=1)
    if get_cur_msg.msg_count < get_message_api:
        get_cur_msg.msg_count = get_message_api
        get_cur_msg.save()
        print 'SAVE'
        sender = 'TEST'
        message = 'Vi poluchili novoe sms soobschenie ot %s, test VK API' % sender
        #requests.get('http://smsc.ru/sys/send.php?login={{login}}&psw={{password}}&{{&phones={{PHONE}} &mes=%s' % message )
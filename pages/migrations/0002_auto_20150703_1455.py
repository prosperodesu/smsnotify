# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('pages', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='message',
            name='attach',
        ),
        migrations.RemoveField(
            model_name='message',
            name='datetime_create',
        ),
        migrations.RemoveField(
            model_name='message',
            name='text',
        ),
        migrations.AddField(
            model_name='message',
            name='msg_count',
            field=models.IntegerField(default=0, max_length=100),
        ),
    ]

from django.views.generic import TemplateView
from controllers import VkApi
class BaseView(TemplateView):

    api_vk = VkApi()
    def get_context_data(self, **kwargs):
        context = super(BaseView, self).get_context_data(**kwargs)
        context['msg_count'] = self.api_vk.get_msg_count()
        print self.api_vk.cur_user_online()
        return context

class HomeView(BaseView):
    template_name = "index.html"

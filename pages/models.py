from django.db import models

class Message(models.Model):
    msg_count = models.IntegerField(max_length=100, default=0)
from django.conf.urls import include, url, patterns
from django.contrib import admin
from pages.views import HomeView

urlpatterns = patterns('pages.views',
    url(r'^$', HomeView.as_view()),

    )
import vk_api
from django.conf import settings
import requests

class VkApi():
    def __init__(self):
        self.vk = vk_api.VkApi(settings.VK_LOGIN, settings.VK_PASS)
        try:
            self.vk.authorization()
        except vk_api.AuthorizationError as error_msg:
            print(error_msg)

    def get_msg_count(self):
        dialogs = self.vk.method('messages.get')
        msg_count = dialogs['count']
        return int(msg_count)

    def cur_user_online(self, id=297679027):
        filter = {'fields': 'online_mobile,online'}
        get_user = self.vk.method('users.get', filter)
        for item in get_user:
            print item['online']
